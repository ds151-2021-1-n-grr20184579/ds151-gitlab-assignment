import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { AuthProvider } from './src/context/AuthContext';
import LoginScreen from './src/screens/LoginScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import LogoutScreen from './src/screens/LogoutScreen';
import { navigationRef } from './RootNavigation';
import ProjectsScreen from './src/screens/ProjectsScreen';
import TabMenu from './src/components/TabMenu';

const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function Home() {
	return (
		<Drawer.Navigator initialRouteName="Home">
			<Drawer.Screen name="Perfil" component={Profile} />
			<Drawer.Screen name="Logout" component={LogoutScreen} />
		</Drawer.Navigator>
	);
}

function Profile() {
	return (
		<Tab.Navigator tabBar={props => <TabMenu {...props} />}>
			<Tab.Screen name="Perfil" component={ProfileScreen} />
			<Tab.Screen name="Projetos" component={ProjectsScreen} />
		</Tab.Navigator>
	)
}

export default function App() {
	return (
		<AuthProvider>
			<NavigationContainer ref={navigationRef}>
				<Stack.Navigator headerMode='none'>
					<Stack.Screen name="Login" component={LoginScreen} />
					<Stack.Screen name="Home" component={Home} />
				</Stack.Navigator>
			</NavigationContainer>
		</AuthProvider>
	);
}