import React, { useContext, useState, useEffect } from 'react'
import { View, StyleSheet } from 'react-native'
import { Text, Input, Button, SocialIcon } from 'react-native-elements'
import { AuthContext } from '../context/AuthContext'
import { SafeAreaView } from 'react-native-safe-area-context';
import { TouchableOpacity } from 'react-native-gesture-handler';

const LoginScreen = ({ navigation }) => {

	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');
	const { authState, signIn, tryLocalSignIn } = useContext(AuthContext);

	useEffect(() => {
		tryLocalSignIn();
	})

	return (
		<SafeAreaView style={styles.content}>
			<Text style={styles.title}>
				Entre com sua conta Gitlab
			</Text>
			<Input
				inputContainerStyle={{ borderBottomWidth: 0 }}
				containerStyle={{ paddingHorizontal: 0 }}
				style={styles.input}
				placeholder="usuário"
				onChangeText={(value) => setUsername(value)}
				value={username}
			/>
			<Input
				inputContainerStyle={{ borderBottomWidth: 0 }}
				containerStyle={{ paddingHorizontal: 0 }}
				style={styles.input}
				placeholder="senha"
				onChangeText={(value) => setPassword(value)}
				value={password}
				secureTextEntry={true}
			/>
			<TouchableOpacity
				style={styles.button}
				onPress={() => {
					signIn({ username, password });
				}}
			>
				<Text style={styles.buttonText}>Entrar</Text>
			</TouchableOpacity>
			{authState.error ? <Text>{authState.error}</Text> : null}
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	content: {
		margin: 30,
		flex: 1,
		justifyContent: 'center',
	},
	title: {
		alignSelf: 'center',
		padding: 5,
		marginBottom: 30,
		fontSize: 20,
		fontWeight: '700',
		color: 'teal'
	},
	input: {
		borderWidth: 1,
		borderRadius: 8,
		borderColor: 'teal',
		paddingVertical: 8,
		paddingHorizontal: 16,
	},
	button: {
		width: '100%',
		backgroundColor: 'teal',
		paddingVertical: 12,
		borderRadius: 8,
		alignItems: 'center'
	},
	buttonText: {
		color: 'white',
		fontWeight: '700',
		fontSize: 18
	}
})

export default LoginScreen;