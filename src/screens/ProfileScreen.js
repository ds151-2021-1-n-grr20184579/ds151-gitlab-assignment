import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, Image } from 'react-native'
import { Text } from 'react-native-elements'
import { AuthContext } from '../context/AuthContext'
import gitlab from '../api/gitlab'
import { SafeAreaView } from 'react-native-safe-area-context';
import { View } from 'react-native'


const ProfileScreen = ({ navigation }) => {
	const { authState } = useContext(AuthContext);
	const [name, setName] = useState('');
	const [avatar, setAvatar] = useState('');

	useEffect(() => {
		async function profile() {
			const response = await gitlab.get(`/users/${authState.id}`);
			setName(response.data.name);
			setAvatar(response.data.avatar_url);
		}

		profile();
	}, [])

	return (
		<SafeAreaView style={styles.content}>
			<Text style={styles.title}>
				Usuário logado
			</Text>
			<View>
				<Image
					style={styles.avatar}
					source={{ uri: avatar }}
				/>
				<Text style={styles.title}>
					{name}
				</Text>
			</View>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	content: {
		margin: 30,
		flex: 1,
	},
	title: {
		alignSelf: 'center',
		padding: 5,
		marginBottom: 30,
		fontSize: 20,
		fontWeight: '700',
		color: 'teal'
	},
	avatar: {
		width: '100%',
		aspectRatio: 1,
		borderRadius: 8,
		marginBottom: 30,
	}
})

export default ProfileScreen;