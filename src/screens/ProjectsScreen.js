import React, { useContext, useState, useEffect } from 'react'
import { StyleSheet, ScrollView } from 'react-native'
import { Text, ListItem } from 'react-native-elements'
import { AuthContext } from '../context/AuthContext'
import gitlab from '../api/gitlab'
import { SafeAreaView } from 'react-native-safe-area-context';


const ProjectsScreen = ({ navigation }) => {
	const { authState } = useContext(AuthContext);
	const [projects, setProjects] = useState([]);

	useEffect(() => {
		async function projects() {
			const response = await gitlab.get(`/users/${authState.id}/projects`)
			setProjects(response.data)
		}

		projects();
	}, [])

	return (
		<SafeAreaView style={styles.content}>
			<Text style={styles.title}>
				Projects Screen
			</Text>

			<ScrollView>
				{projects.map(projects => {
					return (
						<ListItem
							key={projects.id}
							containerStyle={styles.listItem}
						>
							<Text style={styles.listItemText}>
								{projects.name}
							</Text>
						</ListItem>
					)
				})}
			</ScrollView>
		</SafeAreaView>
	)
}

const styles = StyleSheet.create({
	content: {
		margin: 30,
		flex: 1,
	},
	title: {
		alignSelf: 'center',
		padding: 5,
		marginBottom: 30,
		fontSize: 20,
		fontWeight: '700',
		color: 'teal'
	},
	listItem: {
		borderRadius: 8,
		paddingVertical: 16,
		paddingHorizontal: 24,
		backgroundColor: 'teal',
		marginBottom: 16,
	},
	listItemText: {
		color: 'white',
		fontSize: 16,
	}
})

export default ProjectsScreen;