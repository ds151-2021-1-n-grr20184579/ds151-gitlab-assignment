import React, { createContext, useReducer } from 'react';
import axios from 'axios'
import * as RootNavigation from '../../RootNavigation'
import AsyncStorage from '@react-native-async-storage/async-storage';
import gitlab from '../api/gitlab'

const AuthContext = createContext(null);


// Aqui vai definir o que usuário fez
function authReducer(state, action) {
	switch (action.type) {
		case "signIn": // Logou e altera o contexto
			return ({
				...state,
				signedIn: true,
				access_token: action.payload,
				id: action.id,
				username: action.username,
				error: null
			})
		case "error": // Alguma coisa deu errado, vai saber o que
			return ({
				...state,
				error: action.payload
			})
		case "signOut": // Log Out
			return ({
				...state,
				signedIn: false,
				access_token: null,
				id: null,
				username: '',
				error: null
			})
		default:
			return ({ ...state });
	}
}

const AuthProvider = ({ children }) => {

	// Reducer 
	// coloquei ID e Username pq eram precisos para consultar projetos e os dados do usuario
	const [authState, dispatch] = useReducer(authReducer, {
		signedIn: false,
		access_token: null,
		id: null,
		username: '',
		error: ''
	});

	const tryLocalSignIn = async () => {

		// Verifica se ja esta logado
		const access_token = await AsyncStorage.getItem('access_token');
		const username = await AsyncStorage.getItem('username');
		const id = await AsyncStorage.getItem('id');
		if (access_token) {
			dispatch({ type: 'signIn', payload: access_token, username: username, id: id })
			RootNavigation.navigate("Home");
		} else {
			dispatch({ type: 'signOut' })
			RootNavigation.navigate("Login");
		}
	}
	const signIn = async ({ username, password }) => {
		try {
			// Faz a requisição para pegar o token
			const response = await axios({
				method: 'post',
				url: 'https://gitlab.com/oauth/token',
				data: {
					grant_type: "password",
					username,
					password,
				}
			})
			// Essa parte é a que pegamos o id do usuário, aparentemente retorna um array no data
			// Não sei se teria outra forma melhor pra fazer isso '-'
			const res = await gitlab.get(`/users?username=${username}`);
			const id = res.data[0].id;

			// Guarda os dados pegos no AsyncStorage
			await AsyncStorage.setItem('access_token', response.data.access_token);
			await AsyncStorage.setItem('username', username);
			await AsyncStorage.setItem('id', id.toString());

			// Salva os dados
			dispatch({ type: 'signIn', payload: response.data.access_token, username: username, id: id });
			RootNavigation.navigate('Home');
		} catch (error) {
			dispatch({ type: 'error', payload: 'Problemas na autenticação do usuario' })
		}
	}

	const signOut = async () => {
		try {
			await AsyncStorage.removeItem('access_token');
			await AsyncStorage.removeItem('username');
			await AsyncStorage.removeItem('id');
			dispatch({ type: 'signOut' });
		} catch (error) {
			console.log(error);
		}
	}

	return (
		// Provider com as funções que usamos
		<AuthContext.Provider value={{
			authState,
			signIn,
			signOut,
			tryLocalSignIn
		}}>
			{children}
		</AuthContext.Provider>
	);
}

export { AuthContext, AuthProvider };