import React from 'react';
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'

const TabMenu = ({ state, descriptors, navigation }) => {
	return (
		<View style={styles.content}>
			{state.routes.map((route, index) => {
				const { options } = descriptors[route.key];
				const label =
					options.tabBarLabel !== undefined
						? options.tabBarLabel
						: options.title !== undefined
							? options.title
							: route.name;

				const isFocused = state.index === index;

				const onPress = () => {
					const event = navigation.emit({
						type: 'tabPress',
						target: route.key,
					});

					if (!isFocused && !event.defaultPrevented) {
						navigation.navigate(route.name);
					}
				};

				const onLongPress = () => {
					navigation.emit({
						type: 'tabLongPress',
						target: route.key,
					});
				};

				return (
					<TouchableOpacity
						key={index}
						accessibilityRole="button"
						accessibilityStates={isFocused ? ['selected'] : []}
						accessibilityLabel={options.tabBarAccessibilityLabel}
						testID={options.tabBarTestID}
						onPress={onPress}
						onLongPress={onLongPress}
						style={[styles.button, isFocused && styles.active]}
					>
						<Text style={{ fontWeight: '700', color: isFocused ? 'white' : 'teal' }}>
							{label}
						</Text>
					</TouchableOpacity>
				);
			})}
		</View>
	);
}

const styles = StyleSheet.create({
	content: {
		flexDirection: 'row',
		justifyContent: "center",
		alignItems: "center",
		margin: 30,
		borderRadius: 8,
		borderWidth: 1,
		borderColor: 'teal',
		overflow: 'hidden'
	},
	button: {
		flex: 1,
		alignItems: "center",
		paddingVertical: 12,
	},
	active: {
		backgroundColor: 'teal'
	}
})


export default TabMenu;